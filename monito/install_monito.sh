#!/bin/bash

## install master consul

IP=$(hostname -I | awk '{print $2}')
echo "START - install prometheus - "$IP

echo "[1]: install utils and prometheus"
apt-get update -qq >/dev/null
apt-get install -qq -y wget unzip prometheus >/dev/null

echo "[3]: prometheus conf"
echo "
global:
  scrape_interval:     5s 
  evaluation_interval: 5s 
  external_labels:
    monitor: 'codelab-monitor'
rule_files:
scrape_configs:
  - job_name: prometheus
    static_configs:
      - targets: ['localhost:9090']
  - job_name: node_exporter
    static_configs:
      - targets: ['192.168.59.3:9100']

" >/etc/prometheus/prometheus.yml
service prometheus restart
systemctl enable prometheus

echo "[4]: install grafana"

sudo wget -q -O - https://packages.grafana.com/gpg.key | apt-key add -
sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
apt-get update -qq >/dev/null
apt-get install -qq -y grafana >/dev/null
service grafana-server start
systemctl enable grafana-server

sleep 10
curl 'http://localhost:3000/api/datasources' -X POST -H 'Content-Type: application/json;charset=UTF-8' --data-binary '{"name":"Prometheus","type":"prometheus","url":"http://prometheus:9090","access":"proxy","isDefault":true}'

echo "[4]: install node exporter"

sudo useradd -rs /bin/false node_exporter
wget https://github.com/prometheus/node_exporter/releases/download/v0.18.1/node_exporter-0.18.1.linux-amd64.tar.gz
tar -xvzf node_exporter-0.18.1.linux-amd64.tar.gz
mv node_exporter-0.18.1.linux-amd64/node_exporter /usr/local/bin/
chown node_exporter:node_exporter /usr/local/bin/node_exporter

echo "
[Unit]
Description=Node Exporter
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter

[Install]
WantedBy=multi-user.target
" > /etc/systemd/system/node_exporter.service

systemctl daemon-reload
systemctl enable node_exporter
systemctl restart node_exporter

echo "END - install prometheus / grafana"

